package masdar.jh;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JhBotExample {
    // SET THIS VARIABLE
    private static final String TOKEN = "YOUR_TOKEN_HERE";
    //////////////////////////////////////////////////////////////////////////////////////
    // Set the server host (most probably you do not need to change this, but it should be www.juniorhighgame.com)
    // Unless you are running your own jhg server
    private static final String SERVER_HOST = "http://www.juniorhighgame.com";
    //private static final String SERVER_HOST = "http://localhost:5000";
    //////////////////////////////////////////////////////////////////////////////////////

    private static ArrayList< HashMap<String, Integer> > history = new ArrayList<HashMap<String, Integer>>();

    private static JhBotApi api = new JhBotApi(SERVER_HOST, TOKEN);

    public static void main(String[] args) {
        System.out.println("Program started.");


        JSONArray games = api.getGamesList();

        System.out.println("Listing available games\n---------------");
        for(int i=0; i<games.length(); i++) {
            JSONObject game = games.getJSONObject(i);
            System.out.println(game.optString("name") + " id: " + game.optString("id"));
        }
        System.out.println("---------------");
        long gameId;
        if(games.length() == 0 ) {
            System.out.println("No games available. Creating one");
            gameId = api.createGame("It's alive!", 25, 90, 10);
        }
        else {
            gameId = games.optJSONObject(0).optLong("id");
        }

        if (gameId < 0) {
            System.out.println("Error creating a new game or joining an old one. Please check the log");
            System.exit(1);
        }

        // GAME JOINING PART IS HERE

        System.out.println("Joining the first game in the list ("+gameId+"). Was joining successful:"
                + api.joinGame(gameId));
        // END GAME JOINING APRT
        JSONObject prevGameState = api.getGameState(gameId);
        while(true) {
            JSONObject newGameState = api.getGameState(gameId);
            if (newGameState == null) {
                System.out.println("State is null and that is not ok. Please check the code");
            }
            else {
                if(prevGameState != null) {
                    // If the game has not started yet
                    if (newGameState.getInt("state") == JhBotApi.STATE_NOT_STARTED) {
                        // Get number of joined users so far. If more than 1 then start the game.
                        // You can modify the number accordingly
                        int minimumNumberOfUsersJoinedToStartTheGame = 2;
                        if(newGameState.optJSONArray("users") != null && newGameState.optJSONArray("users").length() >= minimumNumberOfUsersJoinedToStartTheGame) {
                            System.out.println("Somebody joined, starting the game");
                            api.startGame(gameId);
                        }
                    }
                    else if (newGameState.getInt("state") == JhBotApi.STATE_FINISHED) {
                        System.out.println("Game has finished");
                        break;
                    }
                    else if (newGameState.getInt("state") == JhBotApi.STATE_IN_PROGRESS) {
                        if (prevGameState.getInt("state") == JhBotApi.STATE_NOT_STARTED) {
                            System.out.println("Yahoo! The game has started!");
                        }
                        if(prevGameState.getInt("round") < newGameState.getInt("round")) {
                            // New round has started
                            HashMap<String, Integer> transactions = new HashMap<String, Integer>();
                            try {
                                // Lets try to run your function
                                transactions = makeMove(newGameState);
                            }
                            catch (Exception ex) {
                                // If execution reached this part then you did something wrong in makeMove method!
                                // Try harder, debug and try again!
                                // DO NOT instantly mail me or the TA. Think and google before that.
                                // Anyway, if after an hour of debugging you still have no idea why your code does not work
                                // feel free to send me an email with a PayPal payment of 1$ (attaching this whole file)
                                // to hrafael@masdar.ac.ae
                                System.err.println("There was an error while invoking makeMove function.\nPlease check it.\n This will be accounted as you have kept all the tokens to yourself\nThe stack trace of the error is presented below");
                                ex.printStackTrace();
                            }
                            finally {
                                try {
                                    boolean isMoveOk = api.makeMove(gameId, transactions);
                                    if(isMoveOk)
                                        System.out.println("Last move was successful");
                                    else
                                        System.out.println("Last move failed! Check previous logs for more info on the error");
                                }
                                catch (Exception ex) {
                                }

                            }
                        }
                    }
                }
                prevGameState = newGameState;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Program finished. So long and thank you for all the fish!");
    }

    private static HashMap<String ,Integer> recordHistory(JSONObject state) {
        HashMap<String ,Integer> receivedTokens = new HashMap<String, Integer>();
        try {
            JSONArray users = state.optJSONArray("users");
            // Here is how you access it
            for(int i=0; i<users.length(); i++) {
                JSONObject user = users.optJSONObject(i);
                if (!user.optBoolean("isCurrentPlayer")){
                    receivedTokens.put(user.optString("id"), user.optInt("received"));
                }
            }
            // Lets record the history. You might need it in future
            history.add(receivedTokens);
        }
        catch (Exception ex) {
            System.out.println("Sadly, there was an exception while recording hte history. Here is the stack trace");
            ex.printStackTrace();
        }
        return receivedTokens;
    }

    /**
     * You need to fill out this function to make move. This function will be called each time a new round starts.
     * @param state current state of the game will be passed
     * @return the transaction hashmap
     * @throws Exception
     */
    private static HashMap<String, Integer> makeMove(JSONObject state) throws Exception {
        // Fill out this variable to make transactions
        HashMap<String, Integer> transactionsToDo = new HashMap<String, Integer>();

        // This array contains the users and the amount of tokens received from them on last round
        JSONArray users = state.optJSONArray("users");

        // This variable will contain available tokens for this round
        int availableTokens = state.optInt("availableToks");

        HashMap<String ,Integer> receivedTokens = recordHistory(state);
        // receivedTokens HashMap contains received tokens (maps user id to tokens got from that user)
        // this.history variable is an array of this kind of objects, representing the history during the game.
        /////////////// START YOUR CODE FROM HERE /////////////////////////////


        // Lets write a simple algorithm to play tit-for-tat
        for(String userId : receivedTokens.keySet()) {
            if (availableTokens - Math.abs(receivedTokens.get(userId)) >= 0) {
                transactionsToDo.put(userId, receivedTokens.get(userId));
                availableTokens -= Math.abs(receivedTokens.get(userId));
            }
        }


        /////////////// END YOUR CODE HERE /////////////////////////////
        // By this point transactionsToDo must contain all transactions you want to do during this move
        return transactionsToDo;
    }
}
