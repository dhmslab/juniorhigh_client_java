package masdar.jh;

import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class HttpRequestHelper {

    String authToken = null;
    String basePath;

    public Response doRequest(String method, String targetEndpoint, String data) {
        HttpURLConnection connection = null;
        try {
            URL url;

            if (data == null) {
                data = "";
            }
            url = new URL(basePath+targetEndpoint);
            connection = (HttpURLConnection)url.openConnection();

            connection.setRequestMethod(method);

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Language", "en-US");

            if (!"GET".equalsIgnoreCase(method)) {
                connection.setRequestProperty("Content-Length", Integer.toString(data.getBytes().length));
            }

            connection.setRequestProperty("authorization", "Bearer "+authToken);

            connection.setUseCaches(false);
            if (!"GET".equalsIgnoreCase(method) && !"DELETE".equalsIgnoreCase(method)) {
                connection.setDoInput(true);
            }

            //Send request
            if (!"GET".equalsIgnoreCase(method) && !"DELETE".equalsIgnoreCase(method)) {
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(data);
                wr.flush();
                wr.close();
            }
            //Get Response
            StringBuilder response = new StringBuilder();
            JSONObject retJson =  new JSONObject();

            try {
                connection.getResponseCode();
                InputStream is = connection.getErrorStream();
                if (is == null) {
                    is = connection.getInputStream();
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append("\n");
                }
                rd.close();
                if (connection.getResponseCode() < 300) {
                    retJson = new JSONObject(response.toString());
                }
                else {
                    System.out.println("Strange response from the server. " +
                            "Your data "+data + "\n" + connection.getResponseCode() +" Response:"+response.toString()+"\n"+
                            "------------------\nReport this to hrafael@masdar.ac.ae with subject JHG ERROR\n------------");
                }
            }
            catch (Exception ex) {
                //ex.printStackTrace();
            }
            return new Response(connection.getResponseCode(), retJson);

        } catch (Exception e) {
            e.printStackTrace();
            return new Response(0, null);

        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
    }

    public HttpRequestHelper(String basePath, String authToken) {
        this.authToken = authToken;
        this.basePath = basePath;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public static String mapToString(Map<String, String> map) {
        String prefix = "";
        StringBuilder stringFormData = new StringBuilder();
        for(String key : map.keySet()) {
            if (map.get(key) != null) {
                stringFormData.append(prefix);
                try {
                    stringFormData.append(key).append("=").append(URLEncoder.encode(map.get(key), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                prefix = "&";
            }
        }
        return stringFormData.toString();
    }
}
